from django import forms
from employe.models import Employe


class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employe
        fields = "__all__"
